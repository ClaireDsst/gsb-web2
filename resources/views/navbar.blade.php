<nav class="navbar navbar-expand-lg site-navbar navbar-light bg-light" id="pb-navbar">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/">Galaxy Swiss Bourdin</a>
    <div class="collapse navbar-collapse justify-content-md-center" id="navbars">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link active" href="/">Accueil</a></li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Prévention
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{@route('getArticles', ['maladie' => 'sida'])}}">SIDA</a>
                    <a class="dropdown-item" href="{{@route('getArticles', ['maladie' => 'hepatite'])}}">Hépatite</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Recherches
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                </div>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{@route('donationPage')}}">Don</a></li>
            <li class="nav-item"><a class="nav-link" href="{{@route('contactPage')}}">Contact</a></li>
        </ul>
    </div>
    <a class="btnSingIn" href="{{@route('signInPage')}}">Se connecter</a> | <a class="btnRegister" href="{{@route('registerPage')}}">S'inscrire</a>
</nav>
