<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>GSB - Galaxy Swiss Bourdin</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.css" rel="stylesheet">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="/css/commun.css" rel="stylesheet">
    <link href="/css/connexion.css" rel="stylesheet">
</head>
<body>
@include('navbar')
<section class="section-connexion">
    <div class="row" id="formConnexion">
        <div class="col-12 d-flex justify-content-center">
            <form id="formCo" method="post" action="/login">
                <div style="margin-bottom: 10px;">
                    <input type="text" name="login" id="login" placeholder="Username..." required>
                </div>
                <div style="margin-top: 10px;">
                    <input type="password" name="psswd" id="psswd" placeholder="Password..." required>
                </div>
                <div style="margin-top: 20px;">
                    <button type="submit" class="btn btn-secondary">Se connecter</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include('footer')
</body>
</html>
