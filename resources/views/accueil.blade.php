<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>GSB - Galaxy Swiss Bourdin</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.css" rel="stylesheet">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="/css/commun.css" rel="stylesheet">
    <link href="/css/home.css" rel="stylesheet">
</head>
<body>
@include('navbar')
    <section class="site-hero" id="section-home" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row intro-text align-items-center justify-content-center">
                <div class="col-md-10 text-center">
                    <h1 class="site-heading site-animate fadeInUp site-animated">Bienvenue chez <strong>GSB</strong></h1>
                    <p class="lead site-subheading mb-4 site-animate fadeInUp site-animated" style="font-size: 28px; color: grey;">Laboratoire d'analyse scientifique</p>
                    <p><a href="#section-about" class="smoothscroll btn px-4 py-3" id="btnMore">Plus sur nous</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section" id="section-about">
    <div class="container">
        <div class="row mb-5 align-items-center">
            <div class="offset-2 col-lg-4 pr-lg-5 mb-5 mb-lg-0">
                <img src="/img/fiole.jpg" alt="Image placeholder" class="img-fluid">
            </div>
            <div class="col-lg-6 pl-lg-5">
                <div class="section-heading">
                    <h2><strong>Présentation</strong></h2>
                </div>
                <p class="lead">Galaxy Swiss Bourdin est un groupe faisant parti de l'industrie pharmaceutique</p>
                <p class="mb-5">Le laboratoire Galaxy Swiss Bourdin (GSB) est issu de la fusion entre le géant américain Galaxy (spécialisé dans le secteur des maladies virales dont le SIDA et les hépatites) et le conglomérat européen Swiss Bourdin (travaillant sur des médicaments plus conventionnels), lui même déjà union de trois petits laboratoires.</p>
            </div>
        </div>
</section>
<section class="section" id="section-history">
    <div class="container">
        <div class="row mb-5 align-items-center">
            <div class="col-lg-12 pl-lg-4">
                <div class="section-heading text-center">
                    <h2>Qui <strong>sommes-nous</strong> ?</h2>
                </div>
                <p class="lead" style="margin-top: 5%;">Notre engagement pour les patients</p>
                <p class="mb-5">Depuis des décennies, GSB s’engage à apporter des solutions innovantes aux patients. Non seulement en mettant des médicaments à leur disposition mais en leur apportant une aide pour la prise en charge globale de leur maladie afin de faciliter leur vie quotidienne.
                    Cet engagement passe aussi par des partenariats avec les associations de patients. De nombreuses associations de patients agissent pour aider les personnes malades et leur entourage. En France, près de 4 millions de personnes sont adhérentes de ces associations, témoignant du rôle essentiel qui est le leur.</p>
                <p class="lead">Notre engagement pour les professionels de santé</p>
                <p class="mb-5">Améliorer la santé de tous, c’est l’ambition de l’ensemble des professionnels de santé. GSB s’engage à leurs côtés : pour les informer, les accompagner dans l’évolution de leur métier ou travailler sur des projets communs.</p>
                <p class="lead">Notre engagement pour la santé publique</p>
                <p class="mb-5">Acteur de santé global, GSB accompagne des actions de santé publique dans ses domaines d’expertises que sont le médicament et les maladies chroniques (en particulier diabète, cancer et prise en charge de la douleur chronique).
                    Notre engagement s’inscrit pleinement dans les politiques de santé nationales et/ou régionales et leurs priorités.</p>
            </div>
        </div>
</section>
    <section class="section" id="section-donation">
        <div class="container">
            <div class="row mb-5 align-items-center">
                <div class="col-lg-12 pl-lg-4">
                    <div class="section-heading text-center">
                        <h2>Un <strong>don</strong></h2>
                    </div>
                    <p style="margin-top: 5%;">Galaxy Swiss Bourdin a besoin de vous !
                        <br> Afin de continuer nos recherches nous avons besoin de financement pour assurer nos frais (achat de matériel, salaire, etc...). Seul vous avez le pouvoir de nous aider.
                        <br> Si vous le souhaitez, vous pouvez faire un don, ponctuel ou régulier, afin de soutenir nos recherches. </p>
                    <p style="text-align: center;"><a href="{{@route('donationPage')}}" class="btn px-4 py-3" id="btnDon">Faire un don</a></p>
                </div>
                <p style="margin-top: 5%;">Galaxy Swiss Bourdin a besoin de vous !
                    <br> Afin de continuer nos recherches nous avons besoin de financement pour assurer nos frais (achat de matériel, salaire, etc...). Seul vous avez le pouvoir de nous aider.
                    <br> Si vous le souhaitez, vous pouvez faire un don, ponctuel ou régulier, afin de soutenir nos recherches. </p>
                <p style="text-align: center;"><a href="{{@route('donationPage')}}" class="btn px-4 py-3" id="btnDon">Faire un don</a></p>
            </div>
    </section>
@include('footer')
</body>
</html>
