<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>GSB - Galaxy Swiss Bourdin</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-grid.css" rel="stylesheet">
    <link href="/css/bootstrap-reboot.css" rel="stylesheet">
    <link href="/css/commun.css" rel="stylesheet">
    <link href="/css/contact.css" rel="stylesheet">
</head>
<body>
@include('navbar')
<section class="section-contact">
    <div class="row" id="formContact">
        <div class="col-12 d-flex justify-content-center">
            <form id="formCont" method="post" action="{{@route('doContact')}}">
                @csrf
                <div style="margin-bottom: 10px;" id="div1">
                    <input type="radio" id="madame" name="civilite" value="Madame"> <label for="Madame" style="margin-right: 5%;">Madame</label>
                    <input type="radio" id="monsieur" name="civilite" value="Monsieur"> <label for="Monsieur" style="margin-right: 5%;">Monsieur</label>
                    <input type="radio" id="none" name="civilite" value="Non precise"> <label>Non précisé</label>
                </div>
                <div style="margin-bottom: 10px;">
                    <input type="text" name="nom" id="nom" placeholder="Votre nom..." required>
                </div>
                <div style="margin-top: 10px;">
                    <input type="text" name="prenom" id="prenom" placeholder="Votre prénom..." required>
                </div>
                <div style="margin-top: 10px;">
                    <input type="email" name="email" id="email" placeholder="Votre email..." required>
                </div>
                <div style="margin-top: 10px;">
                    <textarea name="msg" id="msg" placeholder="Votre message..." required></textarea>
                </div>
                <div style="margin-top: 20px;">
                    <button type="submit" class="btn btn-secondary">Envoyer</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include('footer')
</body>
</html>
