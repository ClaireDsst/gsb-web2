<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<p id="p1">Demande de contact GSB formulée par : {{$civilite}} {{$lastname}} {{$firstname}}</p>
<br>
<p id="p2">E-mail de réponse : {{$email}}</p>
<br>
<p id="p3">Message :</p>
<p id="p4">{{$msg}}</p>
</body>
</html>
