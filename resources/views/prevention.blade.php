<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('head')
<body>
@include('navbar')
<section class="section-article">
        <div class="row">
            <div class="col-12">
                {{$article->titre}}
            </div>
        </div>
    <div class="row">
        <div class="col-12">
            {{$article->contenu}}
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            Dernière modification le :
            @if(is_null($article->datemodif))
                {{$article->dateparution}}
            @endif
        </div>
        <div class="offset-2 col-2">
            Auteur : {{$article->auteur}}
        </div>
    </div>
</section>
<footer class="site-footer fixed-bottom">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 text-center">
                <p>
                    <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                </p>
            </div>
        </div>
        <div class="row" id="rights">
            <p class="col-lg-12 text-center">
                @2020 Tous droits réservés
            </p>
        </div>
    </div>
</footer>
</body>
</html>
