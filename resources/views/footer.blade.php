<footer class="site-footer sticky-bottom">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 text-center">
                <p>
                    <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                </p>
            </div>
        </div>
        <div class="row" id="rights">
            <p class="col-lg-12 text-center">
                @2020 Tous droits réservés
            </p>
        </div>
    </div>
</footer>
