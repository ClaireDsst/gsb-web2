<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('accueil');
});

Route::get("/prevention/{maladie}","PreventionController@findByDisease")->where('maladie', '[A-Za-z]{1,}')->name("getArticles");

Route::get("/recherches/{theme}", "RechercheController@findByTheme")->where('theme', '[A-Za-z]{1,}')->name("getResearch");

Route::get("/contact","ContactController@show")->name("contactPage");

Route::post("/doContact","ContactController@doContact")->name("doContact");

Route::get("/don", "DonationController@show")->name("donationPage");

Route::get("/signIn","ConnexionController@show")->name("signInPage");

Route::get("/register","InscriptionController@show")->name("registerPage");

Route::post("/login","ConnexionController@login")->name("doLogin");
