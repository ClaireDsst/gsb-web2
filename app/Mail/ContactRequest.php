<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $civilite;
    public $firstname;
    public $lastname;
    public $email;
    public $msg;

    /**
     * Create a new message instance.
     *
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $message
     */
    public function __construct(string $civilite, string $firstname, string $lastname, string $email, string $message)
    {
        $this->civilite=$civilite;
        $this->firstname=$firstname;
        $this->lastname=$lastname;
        $this->email=$email;
        $this->msg=$message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mailtestingaddress@gmail.com')
                    ->subject('Demande de contact')
                    ->view('mails.exmpl')->with(['civilite'=>$this->civilite, 'firstname'=>$this->firstname, 'lastname'=>$this->lastname, 'email'=>$this->email, 'message'=>$this->msg]);
    }
}
