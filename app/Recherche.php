<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recherche extends Model
{
    protected $table='recherche';

    protected $primaryKey='article_id';

    public $timestamps = false;
}
