<?php

namespace App\Http\Controllers;

use App\Mail\ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function show() {
        return view('contact');
    }

    public function doContact(Request $request)
    {
        $civilite = $request->get('civilite');
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $email = $request->get('email');
        $msg = $request->get('msg');

        Mail::to('mailtestingaddress@gmail.com')->send(new ContactRequest($civilite, $prenom, $nom, $email, $msg));

        return view('contact');
    }
}
