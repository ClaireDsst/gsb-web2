<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Prevention;

class PreventionController extends Controller
{
    public function find(int $id) {
        $article = Prevention::find($id);
        return $article;
    }

    public function findByDisease(string $disease) {
        $prevention= Prevention::where('maladie', $disease)->get();
        $prevention = $prevention[0];
        $article = Article::find($prevention->id);
        return view('prevention', compact('article', $article));
    }
}
