<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prevention extends Model
{
    protected $table='prevention';

    protected $primaryKey='id';

    public $timestamps = false;
}
